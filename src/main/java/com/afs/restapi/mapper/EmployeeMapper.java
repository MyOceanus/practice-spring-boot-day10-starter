package com.afs.restapi.mapper;

import com.afs.restapi.dto.EmployeeRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;

public class EmployeeMapper {
    public static Employee toEntity(EmployeeRequest employeeRequest){
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeRequest,employee);
        employee.setStatus(true);
        return employee;
    }

    public static Employee toEntity(EmployeeResponse employeeResponse){
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeResponse,employee);
        employee.setStatus(true);
        return employee;
    }

    public static EmployeeResponse toDto(Employee employee){
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee,employeeResponse);
        return employeeResponse;
    }




}
