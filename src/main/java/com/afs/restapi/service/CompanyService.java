package com.afs.restapi.service;

import com.afs.restapi.dto.CompanyRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.mapper.CompanyMapper;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.afs.restapi.mapper.CompanyMapper.toDto;
import static com.afs.restapi.mapper.CompanyMapper.toEntity;
import static java.util.stream.Collectors.toList;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<CompanyResponse> getAll() {
        return companyRepository.findAll()
                .stream()
                .map(CompanyMapper::toDto)
                .collect(toList());
    }

    public List<CompanyResponse> getAll(Integer page, Integer pageSize) {
        return companyRepository.findAll(PageRequest.of(page, pageSize))
                .stream()
                .map(CompanyMapper::toDto)
                .collect(toList());
    }

    public CompanyResponse findById(Integer companyId) {
        return toDto(companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new));
    }

    public CompanyResponse create(CompanyRequest companyRequest) {

        return toDto(companyRepository.save(toEntity(companyRequest)));
    }

    public void deleteCompany(Integer companyId) {
        Optional<Company> company = companyRepository.findById(companyId);
        company.ifPresent(companyRepository::delete);
    }

    public Company update(Integer companyId, Company updatingCompany) {
        Company company = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new);
        if (updatingCompany.getName() != null) {
            company.setName(updatingCompany.getName());
        }
        return companyRepository.save(company);
    }

    public List<EmployeeResponse> getEmployees(Integer companyId) {
        return companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new)
                .getEmployees()
                .stream()
                .map(EmployeeMapper::toDto)
                .collect(toList());
    }
}
