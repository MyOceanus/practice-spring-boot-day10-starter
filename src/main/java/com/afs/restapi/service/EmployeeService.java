package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.afs.restapi.mapper.EmployeeMapper.toDto;
import static com.afs.restapi.mapper.EmployeeMapper.toEntity;
import static java.util.stream.Collectors.toList;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return employeeRepository.findAllByStatusTrue()
                .stream()
                .map(EmployeeMapper::toDto)
                .collect(toList());
    }

    public EmployeeResponse update(int id, EmployeeUpdateRequest employeeUpdateRequest) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
        if (employeeUpdateRequest.getAge() != null) {
            employee.setAge(employeeUpdateRequest.getAge());
        }
        if (employeeUpdateRequest.getSalary() != null) {
            employee.setSalary(employeeUpdateRequest.getSalary());
        }
        employeeRepository.save(employee);
        return toDto(employee);
    }

    public EmployeeResponse findById(int id) {
        return toDto(employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new));
    }

    public List<EmployeeResponse> findByGender(String gender) {
        return employeeRepository.findByGenderAndStatusTrue(gender)
                .stream()
                .map(EmployeeMapper::toDto)
                .collect(toList());
    }

    public List<EmployeeResponse> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return employeeRepository.findAllByStatusTrue(pageRequest)
                .stream()
                .map(EmployeeMapper::toDto)
                .collect(toList());
    }

    public EmployeeResponse insert(EmployeeRequest employeeRequest) {
        return toDto(employeeRepository.save(toEntity(employeeRequest)));
    }

    public void delete(int id) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
